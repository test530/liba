#include <string>
#include <liba/liba.h>
#include <libc/libc.h>

using namespace std::string_literals;
using namespace liba;

void liba::sayHello(std::string_view name)
{
    auto message = "Hello "s;
    message += name;
    message += '\n';
    libc::print(message);
}
