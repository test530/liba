#pragma once

#include <string_view>

namespace liba
{
    void sayHello(std::string_view name);
}
