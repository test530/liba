include_guard(GLOBAL)

# Guarantee that we use the latest FetchContentHelper
set(__FetchContentHelperVer 3)
if (DEFINED FetchContentHelperVer)
    if (FetchContentHelperVer GREATER_EQUAL __FetchContentHelperVer)
        return()
    endif()
endif()
set(FetchContentHelperVer ${__FetchContentHelperVer})
message("FetchContentHelperVer=${FetchContentHelperVer}")

set(FETCHCONTENT_UPDATES_DISCONNECTED ON CACHE BOOL "Disable/enable the update stage")
set(FCH_PATCH_DIR ${CMAKE_CURRENT_LIST_DIR}/patches CACHE INTERNAL "")

function(get_absolute_url_for_git relpath out_var)
    find_package(Git QUIET)
    execute_process(
        COMMAND ${GIT_EXECUTABLE} remote get-url origin
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE base_url
        OUTPUT_STRIP_TRAILING_WHITESPACE
        RESULT_VARIABLE exit_code)
    if(NOT exit_code EQUAL "0")
        message(FATAL_ERROR "Failed to get origin URL for '${CMAKE_CURRENT_SOURCE_DIR}'")
    endif()

    get_filename_component(url ${relpath} ABSOLUTE BASE_DIR ${base_url})
    set(${out_var} ${url} PARENT_SCOPE)
endfunction()

function(is_relative_url url out_var)
    # The relative url must begin with ".."
    string(FIND ${url} ".." pos)
    if (pos EQUAL 0)
        set(${out_var} TRUE PARENT_SCOPE)
    else()
        set(${out_var} FALSE PARENT_SCOPE)
    endif()
endfunction()

function(get_absolute_url name vc url out_var)
    is_relative_url(${url} is_relpath)
    if (NOT is_relpath)
        set(${out_var} ${url} PARENT_SCOPE)
        return()
    endif()

    message("A relative URL has been set for ${name}: ${url}")
    string(TOUPPER ${vc} uvc)
    if (uvc STREQUAL "GIT")
        get_absolute_url_for_git(${url} absolute_url)
    else()
        message(FATAL_ERROR "Relative url not supported for ${vc}")
    endif()

    message("Absolute URL: ${absolute_url}")
    set(${out_var} "${absolute_url}" PARENT_SCOPE)
endfunction()

include(FetchContent)
# let find_package pick up our subproject
# see https://gitlab.kitware.com/cmake/cmake/issues/17735
function(register_source_package name)
    set(svargs INCLUDE_CONTENT)
    set(mvargs INCLUDE)
    cmake_parse_arguments(PARSE_ARGV 1 RSP "" "${svargs}" "${mvargs}")
    set(include "")
    foreach(include_file ${RSP_INCLUDE})
        set(include "${include}\ninclude(${include_file})")
    endforeach()
    file(WRITE
        "${CMAKE_BINARY_DIR}/_pkgs/${name}/${name}Config.cmake"
        ${include}
        ${RSP_INCLUDE_CONTENT}
        )
    set(${name}_DIR ${CMAKE_BINARY_DIR}/_pkgs/${name} CACHE PATH "find_package helper")
    message("Generate package file: ${name}Config.cmake")
    message("${name}_DIR: ${${name}_DIR}")
endfunction()

function(FetchContentHelper name vc url tag)
    set(options ADD_SUBDIR)
    set(mvargs CONFIG_SUBDIR PATCH_SUBDIR REGISTER_PACKAGE)
    cmake_parse_arguments(PARSE_ARGV 4 FCH "${options}" "" "${mvargs}")

    get_absolute_url(${name} ${vc} ${url} url)

    set(declare_args
        ${name}
        ${vc}_REPOSITORY ${url}
        ${vc}_TAG        ${tag}
        GIT_PROGRESS     ON
        )
    if(FCH_PATCH_SUBDIR)
        list(GET FCH_PATCH_SUBDIR 0 patchcmd)
        list(GET FCH_PATCH_SUBDIR 1 patcharg)
        message("Use patch ${patchcmd} ${patcharg}")
        set(declare_args ${declare_args} PATCH_COMMAND ${patchcmd} ${patcharg})
    endif()
    FetchContent_Declare(${declare_args})
    FetchContent_GetProperties(${name})
    if(NOT ${name}_POPULATED)
        message("Setting up ${name} from ${url}")
        FetchContent_Populate(${name})
        set(${name}_SOURCE_DIR ${${name}_SOURCE_DIR} PARENT_SCOPE)
        set(${name}_BINARY_DIR ${${name}_BINARY_DIR} PARENT_SCOPE)
        if(FCH_ADD_SUBDIR)
            foreach(config ${FCH_CONFIG_SUBDIR})
                string(REPLACE "=" ";" configkeyval ${config})
                list(LENGTH configkeyval len)
                if (len GREATER_EQUAL 2)
                    list(GET configkeyval 0 configkey)
                    list(SUBLIST configkeyval 1 -1 configvals)
                    string(REPLACE ";" "=" configval "${configvals}")
                else()
                    message(FATAL_ERROR "Invalid config: ${configkeyval}")
                endif()
                message("Set ${configkey} = ${configval}")
                set(${configkey} ${configval} CACHE INTERNAL "" FORCE)
            endforeach()
            message("${name}_SOURCE_DIR: ${${name}_SOURCE_DIR}")
            message("${name}_BINARY_DIR: ${${name}_BINARY_DIR}")
            add_subdirectory(${${name}_SOURCE_DIR} ${${name}_BINARY_DIR} EXCLUDE_FROM_ALL)
            if (FCH_REGISTER_PACKAGE)
                register_source_package(${FCH_REGISTER_PACKAGE})
            endif()
        endif()
    endif()
endfunction(FetchContentHelper)
